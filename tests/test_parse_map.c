/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_map_validate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 18:37:57 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 17:18:27 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <assert.h>

#include "../src/coord_struct.h"
#include "../src/map.h"
#include "../src/map_struct.h"
#include "../src/string_func.h"

int	main(void)
{
	t_map		*map;
	char		**lines;
	char	*valid0 = "4.o#\n....\n..o.\n....\n";

	lines = get_lines(valid0);
	map = parse_map(lines);
	assert(map->width == 4);
	assert(map->height == 3);
	assert(map->glyph_empty == '.');
	assert(map->glyph_obstacle == 'o');
	assert(map->glyph_full == '#');
	assert(map->obstacles
		&& *map->obstacles[0]->x == 2 && *map->obstacles[0]->y == 1);
}
