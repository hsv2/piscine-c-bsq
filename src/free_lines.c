/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 17:06:18 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 09:33:32 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "free_lines.h"

void	free_lines(char **lines)
{
	unsigned int	i;

	i = 0;
	while (lines[i] != 0)
	{
		free(lines[i]);
		i++;
	}
	free(lines);
}

void	multi_free_lines(char ***lines)
{
	unsigned int	i;

	i = 0;
	while (lines[i] != 0)
	{
		free_lines(lines[i]);
		i++;
	}
	free(lines);
}
