/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_solve.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/20 10:43:17 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 15:07:25 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_solve.h"
#include "map_getter.h"
#include "map_getter_moar.h"
#include "coord_struct.h"

t_coord	*map_is_bloqued_column(t_map *map, int column)
{
	int	y;
	t_coord			**iter;

	y = 0;
	while (y < map_height(map))
	{
		iter = map_obstacles(map);
		while (*iter != 0)
		{
			if ((*iter)->y == y && (*iter)->x == column)
				return (*iter);
		}
		y++;
	}
	return (0);
}

t_coord	*map_is_bloqued_row(t_map *map, int row)
{
	int	x;
	t_coord			**iter;

	x = 0;
	while (x < map_width(map))
	{
		iter = map_obstacles(map);
		while (*iter != 0)
		{
			if ((*iter)->y == row && (*iter)->x == x)
				return (*iter);
		}
		x++;
	}
	return (0);
}
