/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/20 14:22:54 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 16:00:02 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "render.h"
#include "coord_struct.h"
#include "map_getter.h"
#include "map_getter_moar.h"
#include "square_struct.h"
#include "map_solve.h"

struct s_render
{
	int		width;
	int		height;
	char	*rendering;
};

char	*char_at(t_render *render, int x, int y)
{
	return (&render->rendering[y * (render->width + 1) + x]);
}

void	render_obstacles(t_render *render, t_map *map)
{
	t_coord	**iter;

	iter = map_obstacles(map);
	while (*iter != 0)
	{
		*char_at(render, (*iter)->x, (*iter)->y) = map_glyph_obstacle(map);
	}
}

void	fill_square(t_render *render, t_square square, char glyph)
{
	int	x;
	int	y;

	y = square.position.y;
	while (y < square.size)
	{
		x = square.position.x;
		while (x < square.size)
		{
			*char_at(render, x, y) = glyph;
			x++;;
		}
		y++;
	}
}

void init_map(t_render *render, t_map *map)
{
	int	x;
	int	y;

	x = 0;
	y = 0;
	render->rendering = malloc(map_height(map) * (map_width(map) + 1) + 1);
	while (y < render->height)
	{
		while (x < render->width)
		{
			*char_at(render, x, y) = map_glyph_empty(map);
			x++;
		}
		*char_at(render, x, y) = '\n';
		y++;
	}
	*char_at(render, x, y) = 0;
}

t_render	*render_map(t_map *map)
{
	t_render	*render;

	render = malloc(sizeof (t_render));
	init_map(render, map);
	render_obstacles(render, map);
	fill_square(render, *map_biggest_square(map), map_glyph_full);
	return (render);
}

void	free_rendering(t_render *render)
{
	free(render->rendering);
	free(render);
}
