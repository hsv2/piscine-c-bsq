/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_getter_moar.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 15:44:44 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 14:50:02 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_GETTER_MOAR_H
# define MAP_GETTER_MOAR_H

# include "map.h"
# include "coord.h"
# include "square.h"

char		*map_line(t_map *map, int line_nb);
t_coord		**map_obstacles(t_map *map);
t_square	**map_squares(t_map *map);

#endif
