/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 15:40:18 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 15:49:58 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "number_string.h"
#include "math.h"

int	number_len(char *str)


{
	int	len;

	len = 0;
	while (str[len] >= '0' && str[len] <= '9')
		len++;
	return (len);
}

char	*parse_number(char *str, int *n)
{
	char	*start;
	int		exp;

	*n = 0;
	exp = number_len(str) - 1;
	start = str;
	while (*str >= '0' && *str <= '9')
	{
		*n += (*str - '0') * ft_pow(10, exp--);
		str++;
	}
	if (start == str)
		return (0);
	return (str);
}
