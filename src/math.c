/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/17 18:55:03 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 19:33:51 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "math.h"

int	ft_pow(int base, int exp)
{
	int	res;

	res = 1;
	if (exp == 0)
		return (1);
	while (exp-- > 0)
		res *= base;
	return (res);
}
