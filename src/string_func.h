/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_func.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 19:47:11 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/17 14:44:00 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRING_FUNC_H
# define STRING_FUNC_H

int		count_lines(char *text);
char	**get_lines(char *text);
int		line_len(char *str);
int		str_len(char *str);
char	*str_copy(char *dst, char *src);

#endif
