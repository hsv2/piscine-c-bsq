/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_solve.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 22:13:28 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 22:17:16 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_SOLVE_H
# define MAP_SOLVE_H

# include "map.h"
# include "square.h"

t_map		*map_compute_squares(t_map *map);
t_square	*map_biggest_square(t_map *map);

#endif
