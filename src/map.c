/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 10:23:09 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 17:15:25 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "array.h"
#include "coord_struct.h"
#include "free_lines.h"
#include "map.h"
#include "map_struct.h"
#include "map_validate.h"
#include "number_string.h"
#include "string_func.h"

int	parse_header(t_map *map, char *first_line)
{
	int		width;
	char	glyphs[3];
	int		i;

	first_line = parse_number(first_line, &width);
	if (!first_line)
		return (0);
	i = 0;
	while (i < 3 && *first_line != 0)
	{
		glyphs[i] = first_line[i];
		i++;
	}
	if (i != 3)
		return (0);
	map->width = width;
	map->glyph_empty = glyphs[0];
	map->glyph_obstacle = glyphs[1];
	map->glyph_full = glyphs[2];
	return (1);
}

int	parse_obstacles(t_map *map)
{
	int		x;
	int		y;
	int		count;
	t_coord	obstacles[4096];

	y = 0;
	count = 0;
	while (y < map->height)
	{
		x = 0;
		while (x < map->width)
		{
			if (map->lines[y][x] == map->glyph_obstacle)
			{
				obstacles[count].x = x;
				obstacles[count].y = y;
				count++;
			}
			x++;
		}
		y++;
	}
	map->obstacles = malloc((count + 1) * sizeof (t_coord *));
	copy_coord_array(map->obstacles, obstacles, count + 1);
	return (count);
}

int	parse_map_lines(t_map *map, char **lines)
{
	int	height;

	height = 0;
	map->lines = malloc(matrix_size((void **)lines) * sizeof (char *));
	while (lines[height] != 0)
	{
		map->lines[height] = malloc(str_len(lines[height] + 1));
		str_copy(map->lines[height], lines[height]);
		height++;
	}
	if (str_len(lines[--height]))
		return (0);
	map->height = height;
	return (1);
}

t_map	*parse_map(char **lines)
{
	t_map	*map;

	map = malloc(sizeof (t_map));
	*map = g_zero_map;
	if (!parse_header(map, *lines) || *++lines == 0)
	{
		free_map(map);
		return (0);
	}
	if (!parse_map_lines(map, lines))
	{
		free_map(map);
		return (0);
	}
	if (!validate_map_info(map) || !validate_map_line(map))
	{
		free_map(map);
		return (0);
	}
	if (!parse_obstacles(map))
	{
		free_map(map);
		return (0);
	}
	return (map);
}

void	free_map(t_map *map)
{
	void	**iterator;

	iterator = (void **) map->obstacles;
	while (*iterator != 0)
	{
		free((t_coord *) *iterator);
		iterator += sizeof (void **);
	}
	free(map->obstacles);
	iterator = (void **) map->squares;
	while (*iterator != 0)
	{
		free((t_square *) *iterator);
		iterator += sizeof (void **);
	}
	free(map->squares);
	free_lines(map->lines);
	free(map);
}
