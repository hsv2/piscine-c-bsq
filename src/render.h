/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/20 14:22:50 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 14:27:09 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RENDER_H
# define RENDER_H

#include "map.h"

typedef struct s_render	t_render;

t_render	*render_map(t_map *map);
void		free_rendering(t_render *ptr);
const char	*rendered_string(t_render *rendering);

#endif
