/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_string.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 15:40:22 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 15:42:04 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NUMBER_STRING_H
# define NUMBER_STRING_H

int		number_len(char *str);
char	*parse_number(char *str, int *number);

#endif
