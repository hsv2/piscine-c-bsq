/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_lines.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 09:15:27 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 09:33:34 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FREE_LINES_H
# define FREE_LINES_H

void	free_lines(char **lines);
void	multi_free_lines(char ***lines);

#endif
