/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 12:18:18 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 15:40:14 by mpuig-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#include "string_func.h"
#include "free_lines.h"

char	*read_stdin(void)
{
	char			buffer[2048];
	char			*input;
	unsigned int	i;
	int				size_read;

	i = 0;
	size_read = 1;
	while (size_read > 0 && i < sizeof buffer - 1)
		size_read = read(0, &buffer[i++], 1);
	if (i == sizeof buffer - 1)
		return (0);
	buffer[i] = '\0';
	input = (char *)malloc(sizeof(char) * i);
	i = 0;
	while (buffer[i] != '\0')
	{
		input[i] = buffer[i];
		i++;
	}
	return (input);
}

ssize_t	get_file_size(int fd)
{
	char	buffer[256];
	ssize_t	size;
	ssize_t	size_read;

	size = 0;
	size_read = 0;
	size_read = read (fd, buffer, sizeof buffer);
	size = size_read;
	while (size_read > 0)
	{
		size_read = read (fd, buffer, sizeof buffer);
		size += size_read;
	}
	return (size);
}

char	*read_file(char *filename)
{
	ssize_t	size;
	char	*file_content;
	int		fd;
	int		offset;

	if (!filename)
		return (read_stdin());
	file_content = 0;
	fd = open(filename, O_RDONLY);
	if (fd <= 0)
		return (0);
	file_content = malloc(get_file_size(fd) + 1);
	close(fd);
	fd = open(filename, O_RDONLY);
	if (!file_content || !fd)
		return (0);
	offset = 0;
	size = read(fd, file_content + offset, 64);
	while (size > 0)
	{
		offset += size;
		size = read(fd, file_content + offset, 64);
	}
	close(fd);
	return (file_content);
}

char	**read_and_get_lines(char *filename)
{
	char	*file_content;
	char	**lines;

	file_content = read_file(filename);
	if (!file_content)
		return (0);
	lines = get_lines(file_content);
	free(file_content);
	return (lines);
}

char	***multi_read_and_get_lines(char **filenames, unsigned int len)
{
	unsigned int	i;
	char			***lines;

	i = 0;
	lines = malloc((len + 1) * sizeof (char *));
	while (i < len)
	{
		lines[i] = read_and_get_lines(filenames[i]);
		i++;
	}
	lines[i] = 0;
	return (lines);
}
