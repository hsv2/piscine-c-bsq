/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 09:16:16 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 18:52:14 by mpuig-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "file.h"
#include "free_lines.h"

int	main(int argc, char **argv)
{
	char	***files;

	if (argc < 2)
	{
		read_stdin();
		files = 0;
	}
	else
	{
		files = multi_read_and_get_lines(&argv[1], argc - 1);
	}
	multi_free_lines(files);
	return (EXIT_SUCCESS);
}
