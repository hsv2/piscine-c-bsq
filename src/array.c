/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 17:06:58 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 17:13:45 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "array.h"
#include "coord_struct.h"

unsigned int	matrix_size(void **matrix)
{
	int	size;

	size = 0;
	while (*matrix++ != 0)
		size++;
	return (size);
}

t_coord	**copy_coord_array(t_coord **dst, t_coord *src, unsigned int size)
{
	unsigned int	i;

	i = 0;
	while (i < size - 1)
	{
		dst[i] = malloc(sizeof (t_coord));
		*dst[i] = src[i];
		i++;
	}
	dst[i] = 0;
	return (dst);
}
