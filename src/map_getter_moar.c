/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_getter_moard.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 15:46:29 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 14:50:02 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_getter_moar.h"
#include "map_struct.h"

char	*map_line(t_map *map, int line_nb)
{
	return (map->lines[line_nb]);
}

t_coord	**map_obstacles(t_map *map)
{
	return (map->obstacles);
}

t_square	**map_squares(t_map *map)
{
	return (map->squares);
}
