/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_getter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 10:21:40 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 17:40:59 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_getter.h"
#include "map_struct.h"

int	map_width(t_map *map)
{
	return (map->width);
}

int	map_height(t_map *map)
{
	return (map->height);
}

char	map_glyph_empty(t_map *map)
{
	return (map->glyph_empty);
}

char	map_glyph_obstacle(t_map *map)
{
	return (map->glyph_obstacle);
}

char	map_glyph_full(t_map *map)
{
	return (map->glyph_full);
}
