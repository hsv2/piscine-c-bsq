/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_validate.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 18:46:10 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 18:47:39 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_VALIDATE_H
# define MAP_VALIDATE_H

#include "map.h"

int	validate_map_info(t_map *map);
int	validate_map_line(t_map *map);

#endif
