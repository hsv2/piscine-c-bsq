/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square_struct.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 21:38:37 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 21:43:35 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUARE_STRUCT_H
# define SQUARE_STRUCT_H

# include "coord_struct.h"

struct s_square
{
	t_coord			position;
	unsigned int	size;
};

typedef struct s_square	t_square;

#endif
