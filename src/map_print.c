/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mpuig-ma <mpuig-ma@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 13:22:34 by mpuig-ma          #+#    #+#             */
/*   Updated: 2022/04/20 15:04:10 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#include "map_getter.h"
#include "map_struct.h"

/*
void	print_map(t_map *map)
{
	int		i;
	int		j;

	i = 0;
	while (i < map_height(map))
	{
		j = 0;
		while (map_line(map, i)[j] != 0)
			write(1, &map_line(map, i)[j], 1);
		write(1, 10, 1);
	}
	write(1, 10, 1);
}
*/
