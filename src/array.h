/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 17:03:55 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 17:08:27 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_H
# define ARRAY_H

# include "coord.h"

unsigned int	matrix_size(void **matrix);

t_coord	**copy_coord_array(t_coord **dst, t_coord *src, unsigned int size);

#endif
