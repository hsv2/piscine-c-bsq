/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_validate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mpuig-ma <mpuig-ma@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 17:28:31 by mpuig-ma          #+#    #+#             */
/*   Updated: 2022/04/19 19:40:27 by mpuig-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "string_class.h"
#include "map_struct.h"
#include "map.h"

int	validate_map_info(t_map *map)
{
	int		i;
	int		j;
	char	c;

	i = 0;
	while (i < map_height(map))
	{
		j = 0;
		while (map_line(i)[j] != 0)
		{
			c = map_line(map, i)[j];
			if (!ft_is_printable(c))
				return (0);
			if (c != map_glyph_empty(map)
				&& c != map_glyph_obstacle(map)
				&& c != map_glyph_full(map))
				return (0);
		}
		if (j != map_width(map))
			return (0);
		i++;
	}
}

int	validate_map_line(t_map *map)
{
// obstacles
}
