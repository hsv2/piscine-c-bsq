/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/16 12:18:21 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 18:24:19 by mpuig-ma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILE_H
# define FILE_H

char	*read_stdin(void);
char	*read_file(char *filename);
char	**read_and_get_lines(char *filename);
char	***multi_read_and_get_lines(char **filenames, int len);

#endif
