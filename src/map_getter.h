/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_getter.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 10:21:37 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/19 17:28:27 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_GETTER_H
# define MAP_GETTER_H

# include "map.h"

int		map_width(t_map *map);
int		map_height(t_map *map);
char	map_glyph_empty(t_map *map);
char	map_glyph_obstacle(t_map *map);
char	map_glyph_full(t_map *map);

#endif
