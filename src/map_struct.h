/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_struct.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ablanken <ablanken@student.42barcel>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/19 10:26:36 by ablanken          #+#    #+#             */
/*   Updated: 2022/04/20 11:56:26 by ablanken         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_STRUCT_H
# define MAP_STRUCT_H

#include "square.h"
#include "coord.h"

struct s_map
{
	int			width;
	int			height;
	char		glyph_empty;
	char		glyph_obstacle;
	char		glyph_full;
	t_square	**squares;
	t_coord		**obstacles;
	char		**lines;
};

typedef struct s_map	t_map;

static const t_map		g_zero_map = {0, 0, 0, 0, 0, 0, 0, 0};

#endif
