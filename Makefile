CC				:= gcc
RM				:= rm -f
DEFAULT_CFLAGS	:= -Wall -Wextra -Wpedantic -Werror
NAME			:= bsq
SRC_DIR			:= src
BUILD_DIR		:= build
SRC				:= $(SRC_DIR)/main.c \
				$(SRC_DIR)/array.c \
				$(SRC_DIR)/file.c \
				$(SRC_DIR)/free_lines.c \
				$(SRC_DIR)/map.c \
				$(SRC_DIR)/map_getter.c \
				$(SRC_DIR)/map_getter_moar.c \
				$(SRC_DIR)/map_print.c \
				$(SRC_DIR)/map_solve.c \
				$(SRC_DIR)/map_validate.c \
				$(SRC_DIR)/math.c \
				$(SRC_DIR)/number_string.c \
				$(SRC_DIR)/render.c \
				$(SRC_DIR)/string_class.c \
				$(SRC_DIR)/string_func.c
HDR				:= $(SRC_DIR)/array.h \
				$(SRC_DIR)/coord.h \
				$(SRC_DIR)/coord_struct.h \
				$(SRC_DIR)/file.h \
				$(SRC_DIR)/free_lines.h \
				$(SRC_DIR)/map.h \
				$(SRC_DIR)/map_getter.h \
				$(SRC_DIR)/map_getter_moar.h \
				$(SRC_DIR)/map_struct.h \
				$(SRC_DIR)/map_validate.h \
				$(SRC_DIR)/math.h \
				$(SRC_DIR)/number_string.h \
				$(SRC_DIR)/render.h \
				$(SRC_DIR)/square.h \
				$(SRC_DIR)/square_struct.h \
				$(SRC_DIR)/string_class.h \
				$(SRC_DIR)/string_func.h
OBJ				:= $(SRC:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)
.PHONY			: all clean fclean re

$(NAME): $(OBJ)
	$(CC) -o $@ $^

all: $(NAME)

clean:
	$(RM) $(BUILD_DIR)/*.o

fclean: clean
	$(RM) $(NAME)

re: fclean all

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c $(HDR)
	mkdir -p $(BUILD_DIR)
	$(CC) $(DEFAULT_CFLAGS) $(CFLAGS) -o $@ -c $<

norme:
	norminette -R CheckForbiddenSourceHeader
